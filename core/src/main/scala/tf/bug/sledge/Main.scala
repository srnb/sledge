package tf.bug.sledge

import cats.effect._
import cats.parse._

object Main extends IOApp {

  val whitespace: Parser[Unit] = Parser.charIn(" \t\r\n").void
  val whitespaces0: Parser0[Unit] = whitespace.rep.void

  override def run(args: List[String]): IO[ExitCode] =
    IO(println("hello")) >> IO(ExitCode.Success)

}
