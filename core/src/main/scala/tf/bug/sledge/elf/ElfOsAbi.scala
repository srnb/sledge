package tf.bug.sledge.elf

import scodec._
import scodec.codecs._

enum ElfOsAbi {

  case SystemV

  case HpUx

  case NetBsd

  case Linux

  case GnuHurd

  case Solaris

  case Aix

  case Irix

  case FreeBsd

  case Tru64

  case NovellModesto

  case OpenBsd

  case OpenVms

  case NonStopKernel

  case Aros

  case FenixOs

  case CloudAbi

  case StratusVos

}

object ElfOsAbi {

  val codec: Codec[ElfOsAbi] = mappedEnum(uint8, Map(
    ElfOsAbi.SystemV -> 0x00,
    ElfOsAbi.HpUx -> 0x01,
    ElfOsAbi.NetBsd -> 0x02,
    ElfOsAbi.Linux -> 0x03,
    ElfOsAbi.GnuHurd -> 0x04,
    ElfOsAbi.Solaris -> 0x06,
    ElfOsAbi.Aix -> 0x07,
    ElfOsAbi.Irix -> 0x08,
    ElfOsAbi.FreeBsd -> 0x09,
    ElfOsAbi.Tru64 -> 0x0A,
    ElfOsAbi.NovellModesto -> 0x0B,
    ElfOsAbi.OpenBsd -> 0x0C,
    ElfOsAbi.OpenVms -> 0x0D,
    ElfOsAbi.NonStopKernel -> 0x0E,
    ElfOsAbi.Aros -> 0x0F,
    ElfOsAbi.FenixOs -> 0x10,
    ElfOsAbi.CloudAbi -> 0x11,
    ElfOsAbi.StratusVos -> 0x12,
  ))

}
