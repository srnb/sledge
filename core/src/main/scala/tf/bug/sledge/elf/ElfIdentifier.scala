package tf.bug.sledge.elf

import scodec._
import scodec.bits._
import scodec.codecs._

final case class ElfIdentifier(
  objectClass: ElfClass,
  dataEndian: ElfEndian,
  version: ElfVersion,
  osAbi: ElfOsAbi,
  abiVersion: Int, // TODO UByte
)

object ElfIdentifier {

  val codec: Codec[ElfIdentifier] =
    (constant(hex"7f454c46") ~> ElfClass.codec :: ElfEndian.codec :: ElfVersion.codec :: ElfOsAbi.codec :: uint8).as[ElfIdentifier]

}
