package tf.bug.sledge.elf

import scodec._
import scodec.codecs._

enum ElfEndian {

  case None

  case TwosComplementLSB

  case TwosComplementMSB

}

object ElfEndian {

  val codec: Codec[ElfEndian] = mappedEnum(uint8, Map(
    ElfEndian.None -> 0,
    ElfEndian.TwosComplementLSB -> 1,
    ElfEndian.TwosComplementMSB -> 2,
  ))

}
