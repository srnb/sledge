package tf.bug.sledge.elf

import scodec._
import scodec.codecs._

enum ElfClass {

  case None

  case X32

  case X64

}

object ElfClass {

  val codec: Codec[ElfClass] = mappedEnum(uint8, Map(
    ElfClass.None -> 0,
    ElfClass.X32 -> 1,
    ElfClass.X64 -> 2,
  ))

}
