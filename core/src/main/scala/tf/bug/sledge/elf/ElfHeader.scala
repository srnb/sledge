package tf.bug.sledge.elf

import scodec._

final case class ElfHeader(
  identifier: ElfIdentifier,
)

object ElfHeader {

  val codec: Codec[ElfHeader] = (ElfIdentifier.codec).as[ElfHeader]

}
