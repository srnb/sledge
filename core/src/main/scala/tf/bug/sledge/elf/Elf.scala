package tf.bug.sledge.elf

import cats._
import cats.implicits._
import scodec._
import scodec.codecs._

final case class Elf(
  header: ElfHeader,
)

object Elf {

  val codec: Codec[Elf] =
    (ElfHeader.codec).as[Elf]

}
