package tf.bug.sledge.elf

import scodec._
import scodec.codecs._

enum ElfVersion {

  case V1

}
  
object ElfVersion {

  val codec: Codec[ElfVersion] = mappedEnum(uint8, Map(
    ElfVersion.V1 -> 1,
  ))

}
