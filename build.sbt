lazy val core = (project in file("core")).settings(
  organization := "tf.bug",
  name := "sledge-core",
  version := "0.1.0",
  scalaVersion := "3.0.0-M3",
  Compile / mainClass := Some("tf.bug.sledge.Main"),
  libraryDependencies ++= Seq(
    "org.typelevel" %% "cats-core" % "2.3.1",
    "org.typelevel" %% "cats-effect" % "3.0.0-M5",
    "org.typelevel" %% "cats-parse" % "0.3.0",
    "org.scodec" %% "scodec-core" % "2.0.0-M3",
  ),
).enablePlugins(NativeImagePlugin)
